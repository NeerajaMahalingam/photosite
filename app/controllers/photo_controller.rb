class PhotoController < ApplicationController
  def index
    @user = User.where(:id => params[:id])
    @photo = Photo.where(:user_id => params[:id])

    @comment = Array.new

    for photo in  @photo
      @temp = Comment.where(:photo_id => photo.id)
      if (@temp[0].nil?)
        puts 'Nothing'
      else
        @comment += @temp
      end
    end
  end
end
